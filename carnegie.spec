#
# Spec file for carnegie
#
Summary:  A class scheduler.
Name:  carnegie
Version:  0.6
Release:  1
Copyright:  GPL
Group: Networking/Utilities
Source:  http://tunison.net/code/carnegie/carnegie-0.6.tar.gz
URL:  http://tunison.net/code/carnegie
Packager:  John Tunison <nosinut@cmu.edu>

%description
A class scheduler written with the GNOME libraries.  Carnegie will take a series of class numbers, fetch the relevant scheduling information from your college website, and produce all possible schedule combinations available.

%prep
rm -rf $RPM_BUILD_DIR/carnegie
tar xzf $RPM_SOURCE_DIR/carnegie-0.6.tar.gz

%build
cd $RPM_BUILD_DIR/carnegie
make

%install
cp $RPM_BUILD_DIR/carnegie/carnegie /usr/bin
rm /usr/lib/carnegie/*
cp $RPM_BUILD_DIR/carnegie/handlers/* /usr/lib/carnegie

%files
%doc carnegie/README
%doc carnegie/HANDLERS
%doc carnegie/COPYING
%doc carnegie/CHANGELOG
%doc carnegie/TODO
/usr/bin/carnegie
%dir /usr/lib/carnegie
