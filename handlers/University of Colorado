#!/usr/bin/perl

#####################################################################
#                                                                   #
#                   University of Colorado Handler                  #
#              Written by PC Drew <drewpc@colorado.edu>             #
#                      Last Updated: 10/24/1999                     #
#                                                                   #
#############################  NOTES  ###############################
#                                                                   #
# Currently there is only support for Boulder Main Campus.  That's  #
# because the website only supports Boulder Main Campus as of yet.  #
# When it supports other campuses, I'll add support in here.  Also, #
# I'm not sure if the summer session term code is correct.  If you  #
# have that information, please email it to me at the above address #
# so the script will be correct.                                    #
#                                                                   #
#####################################################################

$num_args = $#ARGV + 1;

if($num_args == 1) {
	if($ARGV[0] eq "handlerinfo") {
		print "Fall\tSpring\tSummer\n";
		print "Boulder\n";
	} else {
		print "error: bad arguments\n";
	}
} elsif($num_args == 3) {
	$course = uc($ARGV[0]);				# Get course name from ARGV and make it uppercase.
	$season = ucfirst($ARGV[1]);		# Get season from ARGV and uppercase the first letter.
	$campus = ucfirst($ARGV[2]);		# Get campus from ARGV and uppercase the first letter.
  
  	# If the course is XXXX-XXXX or XXXXXXXX, let it pass.
	if(!(length($course) == 8 || length($course) == 9)) { 
		print "error: course ($course) must be in format XXXX-XXXX. (ex. csci-2270)\n";
	}

	# Setup some variables to generate the string to feed to the website.
	$search = 'singlesearch=Show%20Course';
	$term = '&term_=';
	$singledept = '&singledept=';
	$singlecourse = '&singlenumber=';

	# Get the current year.
	$year = substr(localtime, -4);
	if($season eq "Fall") {
		$code = "7";					# Term code for fall is 7.
	} elsif($season eq "Spring") {
		$year += 1;						# Cause you'll be registering for the next year.
		$code = "1";					# Term code for spring is 1.
	} elsif($season eq "Summer") {
		$code = "4";					# Term code for summer is 4.
	}
	$term .= substr($year, 2) . $code;	# Make the term code (i.e 001 for Spring 2000).

	# Make the postdata variable.
    $course =~ s/-//;
    $singledept .= $dept = substr($course,0,4);
    $singlecourse .= $courseNum = substr($course,4,4);
	$postdata = $search . $term . $singledept . $singlecourse;

	# Actually go the the website and enter the data.
	$site = "http://www.colorado.edu/plus/planner/planner.cgi";
	$data = `echo "$postdata" | lynx -useragent="carnegie scheduler" -source -post_data $site`;

	# If no course was found...
	@lines = split(/(No Courses were found that meet your criteria.)/, $data);
	if($lines[0] eq $data) {
		# This gets rid of some of the quirks that they page has.
   		@lines = split(/src=img\/questionmark.gif/, $data, 3);
	   	@lines = split(/src=img\/questionmark.gif/, $lines[2]);

		# foreach returned value...
		foreach $line (@lines) {  
			# The lectures are in red on the webpage.
			if($line =~ /<font(.*?)color=red/i) {
				$is_lecture = "";
			} else {
				$is_lecture = "\t";
			}

			# Due to the above split, we lost 1 < and 1 > tag.  Then remove all of the HTML.
			$line = "<" . $line . ">";
			$line =~ s/<([^>]*|\n*)>//g;

			@breakup = split(/ /, $line);
			$section = substr($breakup[0], -3);	# Get the section.
			$days = $breakup[3];				# The days the class meets.

			# Some weird quirk I ran into.
			if($breakup[1] eq "M") {
				exit;
			}

			@breakup = split(/([012][0-9][0-9][0-9][AP]M)/, substr($breakup[2], 3));

			if($is_lecture eq "\t") {
				if(lc(substr($breakup[0], 0, 3)) eq "rec") {
					$lecture = "Rec-$section";
				} elsif(lc(substr($breakup[0], 0, 3)) eq "lab") {
					$lecture = "Lab-$section";
				}
			} else {
				$breakup[0] =~ s/&nbsp;/ /g;	# Replace the HTML space codes with spaces.	
				$courseName = $breakup[0];

				$lecture = "Lec-$section";	
			}

			# Convert a 12-hr format time to a 24-hr format time.
			$breakup[1] =~ s/[AP]M//;
			if($& eq "PM" && $breakup[1] lt "1200") {
				$breakup[1] += 1200;
			}
			$startTime = $breakup[1];

			# Convert a 12-hr format time to a 24-hr format time.
			$breakup[3] =~ s/[AP]M//;
			if($& eq "PM" && $breakup[3] lt "1200") {
				$breakup[3] += 1200;
			}
			$endTime = $breakup[3];

			# Print the result.
			print "$is_lecture$dept $courseNum\t$courseName\t$lecture\t$days\t$startTime\t$endTime\n";
		}
	} else {
		# When the course isn't listed...
		print "error: Sorry, $dept $courseNum was not found for the $season season.\n";
	}
} else {
	print "usage: <collegename> <class> <season> <campus>\n";
}
