#ifndef __SCHEDVIEW_H__
#define __SCHEDVIEW_H__

#define ORIGINAL    0
#define RANGE       1
#define EARLYFIRST  2
#define EARLYLAST   3

struct schedinfo {
  GtkWidget *schedlist;
  GtkWidget *schedtable;
  GtkWidget *fe;
  GPtrArray *widgets;
  GtkWidget *exportall;
  int numtitles;
  int begin;
  int end;
  int current;
  int total;
};

void setup_schedview(GPtrArray * classdata);
GPtrArray * install_sched_table(GPtrArray *classdata, 
				GtkWidget *table,
				int begin, int end);
void export_html (GtkWidget *widget, struct schedinfo *si);

#endif
