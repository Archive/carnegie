#include <gnome.h>
#include "class.h"
#include "main.h"
#include "gui.h"
#include "prefs.h"

gchar *global_season;
gchar *global_campus;
gchar *global_handler_path;
gchar *global_handler;
gchar *global_error;
int global_merge;
int global_abbr_sec;

int main(int argc, char **argv)
{
  GtkWidget *app;

  gnome_init("Carnegie", VERSION, argc, argv);
  app = gnome_app_new("Carnegie", "Carnegie");

  gtk_signal_connect(GTK_OBJECT(app),
		     "destroy",
		     GTK_SIGNAL_FUNC(destroy),
		     NULL);

  init_prefs();

  setup_screen(app);
  gtk_widget_show(app);
  gtk_main();

  return 0;
}
