#ifndef __CLASS_H__
#define __CLASS_H__

#include <gnome.h>
#include "timeslot.h"

struct _Class {
  gchar *id;
  gchar *title;
  gchar *section;
  gchar *fullsection;
  gchar *abbrsection;
  gchar *days;
  timeslot begin, end;
};

typedef struct _Class Class;

struct _Block {
  // lec should never be NULL!
  Class* lec;
  // rec is a GPtrArray of Class*, and may be NULL if there are none.
  GPtrArray* rec;
};

typedef struct _Block Block;

GPtrArray *  flatten_blocks(GPtrArray *blist);
void         print_blocks(GPtrArray *gp);
void         print_classes(GPtrArray *gp);
void         print_class(Class *c);
GPtrArray *  get_class(char * classnum);
char *       get_twixt_tab(char *data, int tn);
GPtrArray *  merge_similar_blocks(GPtrArray *blist);

#endif // __CLASS_H__
