#include <gnome.h>
#include <assert.h>
#include "timeslot.h"
#include "class.h"
#include "schedview.h"
#include "timeslot.h"

extern int global_merge;
extern int global_abbr_sec;

void destroy_win(GtkWidget *widget, gpointer cb_data)
{
  gtk_widget_destroy(GTK_WIDGET(cb_data));
}

void prev_sched(GtkWidget *widget, gpointer data)
{
  struct schedinfo *si = data;

  si->current = si->current - 1;
  if(si->current < 0) si->current = 0;
  else gtk_clist_select_row(GTK_CLIST(si->schedlist), si->current, 0);
}

void next_sched(GtkWidget *widget, gpointer data)
{
  struct schedinfo *si = data;

  si->current = si->current + 1;
  if(si->current > si->total) si->current = si->total;
  else gtk_clist_select_row(GTK_CLIST(si->schedlist), si->current, 0);
}

void export_html_dialog(GtkWidget *widget, struct schedinfo *si)
{
  GtkWidget *fe;

  /* Create a new file selection widget */
  fe = gtk_file_selection_new ("Export HTML");
  si->fe = fe;
    
  gtk_signal_connect (GTK_OBJECT (fe), "destroy",
		      (GtkSignalFunc) destroy_win, fe);
  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (fe)->ok_button),
                        "clicked", (GtkSignalFunc) export_html, si);
    
  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (fe)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (fe));
    
  //gtk_file_selection_set_filename (GTK_FILE_SELECTION(fe), "test.html");

  si->exportall = gtk_check_button_new_with_label("Export only currently selected schedule");
  gtk_box_pack_start(GTK_BOX(GTK_FILE_SELECTION(fe)->main_vbox), si->exportall, TRUE, TRUE, 0);
  gtk_widget_show(si->exportall);
  
    
  gtk_widget_show(fe);
}

void export_html (GtkWidget *widget, struct schedinfo *si)
{
  int i, j, k, m, a;
  int begin, end, size;
  GPtrArray *classdata;
  GPtrArray *scheddata;
  Class *c;
  FILE *fp;
  int *fill[5];
  char *text;
  char *fn;
  int exportall;

  fn = gtk_file_selection_get_filename (GTK_FILE_SELECTION (si->fe));
  exportall = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(si->exportall));
  
  begin = si->begin;
  end = si->end;
  size = end-begin;

  scheddata = g_ptr_array_new();
  if(!exportall) {
    for(i=0;i<=si->total;i++)
      g_ptr_array_add(scheddata, 
		      gtk_clist_get_row_data(GTK_CLIST(si->schedlist), i));
  } else {
    g_ptr_array_add(scheddata, 
		    gtk_clist_get_row_data(GTK_CLIST(si->schedlist), si->current));
  }

  fp = fopen(fn, "w");
  assert(fp);

  // build an array that matches the table
  for(i=0; i<5; i++) {
    fill[i] = (int *) malloc(sizeof(int) * size);
    printf("allocated fill[%d] %d\n", i, size);
    assert(fill[i]);
  }
  fflush(stdout);
    
  for(a=0;a<scheddata->len; a++) {
    fprintf(fp, "<p><table border=1>\n");
    fprintf(fp, "<tr><td></td><td>MONDAY</td><td>TUESDAY</td><td>WEDNESDAY</td>\n");
    fprintf(fp, "<td>THURSDAY</td><td>FRIDAY</td></tr>\n");
    
    classdata = flatten_blocks(scheddata->pdata[a]);

    // set default fill to 1
    for(i=0; i<5; i++)
      for(j=0; j<size; j++)
	fill[i][j] = 1;
    
    for(i=begin;i<end;i++) {
      fprintf(fp, "<tr>\n");
      fprintf(fp, "<td>%s</td>\n", timeslot_str(i));
      for(j=MONDAY; j<=FRIDAY; j++) {
	for(k=0; k<classdata->len; k++) {
	  c = classdata->pdata[k];
	  assert(c);
	  if(isDay(c->days, j) && c->begin == i) {
	    if(global_merge) {
	      if(global_abbr_sec) text = c->abbrsection;
	      else text = c->fullsection;
	    } else
	      text = c->section;
	    fprintf(fp, "<td align=\"center\" rowspan=\"%d\">%s<br>%s</td>\n", 
		    c->end - c->begin, c->id, text);
	    for(m=(c->begin - begin); m<(c->end - begin); m++)
	      fill[j][m] = 0;
	  }
	}
	if(fill[j][i-begin]) fprintf(fp, "<td></td>");
	printf("[%d]", fill[j][i-begin]);
      }
      fprintf(fp, "</tr>\n\n");
      printf(" eol\n");
    }
    fprintf(fp, "</table>\n\n\n");
    
  }

  for(i=0; i<5; i++) {
    free(fill[i]);
    fill[i] = NULL;
  }

  fprintf(fp, "<p><small><small>created with "
	  "<a href=\"http://tunison.net/code/carnegie\">carnegie</a></small></small>\n");

  //  g_ptr_array_free(scheddata, FALSE);
  gtk_widget_destroy(si->fe);
  fclose(fp);
}

void sort_activated (GtkWidget *widget, gpointer data)
{
  int whichsort;
  GtkWidget *slist;

  whichsort = GPOINTER_TO_INT(gtk_object_get_user_data(GTK_OBJECT(widget)));

  slist = data;
  gtk_clist_set_sort_column (GTK_CLIST(slist), whichsort);
  gtk_clist_sort (GTK_CLIST(slist));
}

void sched_selection_made(GtkWidget *widget, gint row, gint column,
			  GdkEventButton *event, gpointer data)
{
  GPtrArray *gp;
  GPtrArray *oldwidgets;
  GtkContainer *table;
  struct schedinfo *si;
  int i;
  int b, e;

  si = data;
  oldwidgets = (GPtrArray *)si->widgets;
  table = (GtkContainer *) si->schedtable;

  b = si->begin;
  e = si->end;
  si->current = row;

  assert(oldwidgets->len>0);
  for(i=0;i<oldwidgets->len;i++)
    gtk_container_remove(table, (GtkWidget *) (oldwidgets->pdata[i]));
  g_ptr_array_free(oldwidgets, TRUE);

  gp = gtk_clist_get_row_data(GTK_CLIST(si->schedlist), row);
  gp = flatten_blocks(gp);

  si->widgets = install_sched_table(gp, (GtkWidget *)table, b, e);

}

// returns all the widgets to eventually remove
GPtrArray * install_sched_table(GPtrArray *classdata, 
				GtkWidget *table,
				int begin, int end)
{
  GtkWidget *button;
  Class *c;
  char *buttonlabel;
  int i, j;
  GPtrArray *widgetstack;
  GtkTooltips *tooltips;
  char *buttontext;

  widgetstack = g_ptr_array_new();

  for(i=MONDAY; i<=FRIDAY; i++) {
    for(j=0; j<classdata->len; j++) {
      c = classdata->pdata[j];
      if(isDay(c->days, i)) {
	if(global_merge) {
	  if(global_abbr_sec) buttontext = c->abbrsection;
	  else buttontext = c->fullsection;
	} else
	  buttontext = c->section;
	buttonlabel = g_strconcat(c->id, "\n", buttontext, NULL);
	button = gtk_button_new_with_label(buttonlabel);
	gtk_table_attach_defaults(GTK_TABLE(table), button, i+1, i+2, 
				  c->begin+1-begin, c->end+1-begin);
	gtk_widget_show(button);

	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(tooltips, button, c->fullsection, NULL);
	gtk_tooltips_enable(tooltips);

	g_ptr_array_add(widgetstack, button);	
      }
    }
  }

  return widgetstack;
}

void populate_schedlist(struct schedinfo *si, GPtrArray *scheddata)
{
  GtkWidget *slist = si->schedlist;
  int numtitles = si->numtitles;
  int i, j, k;
  char **slist_line;
  int sort_begin, sort_end;
  Block *b;
  Class *c;
  GPtrArray *cgp;
  GPtrArray *bl;
  GPtrArray *flat_classes;
  char tmp[100];
  char c1, c2;
  int bitmask = 255;

  for(i=0;i<scheddata->len; i++) {
    slist_line = (char **) malloc(sizeof(char *)* (numtitles + 4));
    bl = (GPtrArray *) (scheddata->pdata[i]);

    for(j=0;j<numtitles;j++) {
      b = (Block *)bl->pdata[j];
      cgp = (GPtrArray *) b->rec;
      if(cgp==NULL) c = (Class *) b->lec;
      else {
	assert(cgp->len);
	c = (Class *) cgp->pdata[0];
      }
      if(global_merge) {
	if(global_abbr_sec)
	  slist_line[j+4] = c->abbrsection;
	else
	  slist_line[j+4] = c->fullsection;
      } else
	slist_line[j+4] = c->section;
      
    }
    //dirty trick here:  make the text a char, not a digit so that we 
    //can use the builtin clist sorts.
    sprintf(tmp, "%c (%d)", i+65, i);
    slist_line[0] = g_strdup(tmp);

    flat_classes = flatten_blocks(bl);
    c = flat_classes->pdata[0];
    sort_begin = c->begin;
    sort_end = c->end;
    for(j=0;j<flat_classes->len;j++) {
      c = flat_classes->pdata[j];
      if(c->begin < sort_begin) sort_begin = c->begin;
      if(c->end > sort_end) sort_end = c->end;
    }

    c1 = (((sort_end-sort_begin)&bitmask)>>8) + 65;
    c2 = ((sort_end-sort_begin)&~bitmask) + 65;
    sprintf(tmp, "%c%c (%d)", c1, c2, sort_end-sort_begin);
    slist_line[1] = g_strdup(tmp);
    c1 = ((sort_begin&bitmask)>>8) + 65;
    c2 = ((sort_begin)&~bitmask) + 65;
    sprintf(tmp, "%c%c (%d)", c1, c2, sort_begin);
    slist_line[2] = g_strdup(tmp);
    c1 = ((sort_end&bitmask)>>8) + 65;
    c2 = ((sort_end)&~bitmask) + 65;
    sprintf(tmp, "%c%c (%d)", c1, c2, sort_end);
    slist_line[3] = g_strdup(tmp);

    k = gtk_clist_append( GTK_CLIST(slist), slist_line);
    gtk_clist_set_row_data(GTK_CLIST(slist), k, bl);
  }  
  gtk_clist_select_row(GTK_CLIST(slist), 0, 0);

}

void setup_schedview(GPtrArray * scheddata)
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *okbutton;
  GtkWidget *nextbutton;
  GtkWidget *prevbutton;
  GtkWidget *htmlbutton;
  GtkWidget *label;
  GtkWidget *hbox;
  GtkWidget *buttonhbox;
  GtkWidget *sorthbox;
  GtkWidget *bigvbox;
  GtkWidget *listvbox;
  GtkWidget *slist;
  GtkWidget *sched_sw;
  GtkWidget *scrolled_window;
  GtkWidget *sort_omenu;
  GtkWidget *sort_menu;
  GtkWidget *sort_choice;
  GtkWidget *hsep;

  int i, j;
  char **titles;
  char *total_label;
  Block *b;
  Class *c;
  GPtrArray *cgp;
  GPtrArray *bl;
  GPtrArray *oldwidgetstack;
  GPtrArray *classdata;
  int numtitles;
  struct schedinfo *si;
  char tmp[100];
  int begin, end;

  si = (struct schedinfo *) malloc(sizeof(struct schedinfo));

  //find a sane starting point for begin and end markers
  classdata = (GPtrArray *) flatten_blocks(scheddata->pdata[0]);
  c = (Class *) classdata->pdata[0];
  begin = c->begin;
  end = c->end;

  // find the real begin and end
  for(i=0;i<scheddata->len; i++) {
    classdata = flatten_blocks(scheddata->pdata[i]);
    for(j=0; j<classdata->len; j++) {
      c = classdata->pdata[j];
      if(c->begin < begin) begin = c->begin;
      if(c->end > end) end = c->end;
    }
  }

  //provide a little space
  begin--;
  end++;
  if(begin< 0) begin = 0;
  if(end >= END_SLASH_UNKNOWN) end = END_SLASH_UNKNOWN - 1;

  si->begin = begin;
  si->end = end;
  si->current = 0;
  si->total = scheddata->len - 1;

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width(GTK_CONTAINER(window), 20);
  gtk_window_set_title (GTK_WINDOW (window), "Carnegie: Schedule");

  bigvbox=gtk_vbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(window), bigvbox);
  gtk_widget_show(bigvbox);

  hbox=gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(bigvbox), hbox, TRUE, TRUE, 0);
  gtk_widget_show(hbox);

  listvbox=gtk_vbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(hbox), listvbox, FALSE, FALSE, 5);
  gtk_widget_show(listvbox);

  sorthbox=gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(listvbox), sorthbox, FALSE, FALSE, 0);
  gtk_widget_show(sorthbox);

  scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), 
				 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  //  gtk_widget_set_usize(scrolled_window, 0, 150);
  gtk_box_pack_start(GTK_BOX(listvbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show(scrolled_window);

  sprintf(tmp, "%d", si->total+1);
  total_label = g_strconcat(tmp, " possible schedules", NULL);
  label = gtk_label_new(total_label);
  gtk_box_pack_start(GTK_BOX(listvbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  hsep = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(bigvbox), hsep, FALSE, FALSE, 10);
  gtk_widget_show(hsep);


  bl = (GPtrArray *) (scheddata->pdata[0]);
  numtitles = bl->len;
  titles = (char **) malloc(sizeof(char *)* (numtitles + 4));
  if(!titles) {
    printf("schedview() panic, not enough mem\n");
    printf("\ttried mallocing %d\n", numtitles);
    exit(1);
  }

  titles[0] = "original";
  titles[1] = "range";
  titles[2] = "earliest start";
  titles[3] = "earliest end";
  for(j=0;j < numtitles; j++) {
    b = (Block *)bl->pdata[j];
    cgp = (GPtrArray *) b->rec;
    if(cgp==NULL) c = (Class *) b->lec;
    else {
      assert(cgp->len);
      c = (Class *) cgp->pdata[0];
    }
    titles[j+4] = c->id;
  }
  
  slist = gtk_clist_new_with_titles(numtitles+4, titles);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), 
					slist);
  gtk_clist_set_column_visibility (GTK_CLIST(slist), 0, FALSE);
  gtk_clist_set_column_visibility (GTK_CLIST(slist), 1, FALSE);
  gtk_clist_set_column_visibility (GTK_CLIST(slist), 2, FALSE);
  gtk_clist_set_column_visibility (GTK_CLIST(slist), 3, FALSE);
  gtk_clist_column_titles_passive(GTK_CLIST(slist));
  gtk_clist_column_titles_show(GTK_CLIST(slist));
  gtk_widget_show(slist);  

  // set up the sort option menu
  label = gtk_label_new("Sort by:");
  gtk_box_pack_start(GTK_BOX(sorthbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  sort_omenu = gtk_option_menu_new();
  gtk_box_pack_start(GTK_BOX(sorthbox), sort_omenu, FALSE, FALSE, 0);
  gtk_widget_show(sort_omenu);

  sort_menu = gtk_menu_new();
  sort_choice = gtk_menu_item_new_with_label("original");
  gtk_object_set_user_data (GTK_OBJECT (sort_choice), GINT_TO_POINTER (ORIGINAL));
  gtk_signal_connect(GTK_OBJECT(sort_choice), "activate", 
		     (GtkSignalFunc) sort_activated, slist);
  gtk_menu_append (GTK_MENU (sort_menu), sort_choice);
  gtk_widget_show(sort_choice);

  sort_choice = gtk_menu_item_new_with_label("range");
  gtk_object_set_user_data (GTK_OBJECT (sort_choice), GINT_TO_POINTER (RANGE));
  gtk_signal_connect(GTK_OBJECT(sort_choice), "activate", 
		     (GtkSignalFunc) sort_activated, slist);
  gtk_menu_append (GTK_MENU (sort_menu), sort_choice);
  gtk_widget_show(sort_choice);

  sort_choice = gtk_menu_item_new_with_label("first class");
  gtk_object_set_user_data (GTK_OBJECT (sort_choice), GINT_TO_POINTER (EARLYFIRST));
  gtk_signal_connect(GTK_OBJECT(sort_choice), "activate", 
		     (GtkSignalFunc) sort_activated, slist);
  gtk_menu_append (GTK_MENU (sort_menu), sort_choice);
  gtk_widget_show(sort_choice);

  sort_choice = gtk_menu_item_new_with_label("last class");
  gtk_object_set_user_data (GTK_OBJECT (sort_choice), GINT_TO_POINTER (EARLYLAST));
  gtk_signal_connect(GTK_OBJECT(sort_choice), "activate", 
		     (GtkSignalFunc) sort_activated, slist);
  gtk_menu_append (GTK_MENU (sort_menu), sort_choice);
  gtk_widget_show(sort_choice);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (sort_omenu), sort_menu);


  buttonhbox=gtk_hbox_new(TRUE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(buttonhbox), 10);
  gtk_box_pack_start(GTK_BOX(bigvbox), buttonhbox, FALSE, FALSE, 0);
  gtk_widget_show(buttonhbox);

  okbutton = gnome_stock_button(GNOME_STOCK_BUTTON_CLOSE);
  gtk_box_pack_end(GTK_BOX(buttonhbox), okbutton, FALSE, FALSE, 3);
  gtk_widget_show(okbutton);

  nextbutton = gtk_button_new_with_label("Next Schedule");
  gtk_box_pack_end(GTK_BOX(buttonhbox), nextbutton, FALSE, FALSE, 3);
  gtk_widget_show(nextbutton);

  prevbutton = gtk_button_new_with_label("Previous Schedule");
  gtk_box_pack_end(GTK_BOX(buttonhbox), prevbutton, FALSE, FALSE, 3);
  gtk_widget_show(prevbutton);

  htmlbutton = gtk_button_new_with_label("Export as HTML");
  gtk_box_pack_end(GTK_BOX(buttonhbox), htmlbutton, FALSE, FALSE, 3);
  gtk_widget_show(htmlbutton);

  sched_sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sched_sw), 
				 GTK_POLICY_NEVER, GTK_POLICY_NEVER);
  gtk_box_pack_start(GTK_BOX(hbox), sched_sw, TRUE, TRUE, 0);
  gtk_widget_show(sched_sw);

  table = gtk_table_new(end-begin+1, 6, TRUE);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sched_sw), 
					table);
  gtk_widget_show(table);

  label = gtk_label_new("Monday");
  gtk_table_attach_defaults(GTK_TABLE(table), label, 1, 2, 0, 1);
  gtk_widget_show(label);

  label = gtk_label_new("Tuesday");
  gtk_table_attach_defaults(GTK_TABLE(table), label, 2, 3, 0, 1);
  gtk_widget_show(label);

  label = gtk_label_new("Wednesday");
  gtk_table_attach_defaults(GTK_TABLE(table), label, 3, 4, 0, 1);
  gtk_widget_show(label);

  label = gtk_label_new("Thursday");
  gtk_table_attach_defaults(GTK_TABLE(table), label, 4, 5, 0, 1);
  gtk_widget_show(label);

  label = gtk_label_new("Friday");
  gtk_table_attach_defaults(GTK_TABLE(table), label, 5, 6, 0, 1);
  gtk_widget_show(label);

  for(i = 0; i<end-begin; i++) {
    label = gtk_label_new(timeslot_str(begin + i));
    gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_RIGHT);
    gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, i+1, i+2);
    gtk_widget_show(label);
  }

  assert(scheddata->len>0);
  oldwidgetstack = install_sched_table(flatten_blocks(scheddata->pdata[0]),
				       table, begin, end);

  si->schedlist = slist;
  si->numtitles = numtitles;
  si->schedtable = table;
  si->widgets = oldwidgetstack;

  populate_schedlist(si, scheddata);


  gtk_signal_connect(GTK_OBJECT(slist), "select_row",
		     GTK_SIGNAL_FUNC(sched_selection_made),
		     si);
  
  gtk_signal_connect (GTK_OBJECT (htmlbutton), "clicked",
		      GTK_SIGNAL_FUNC (export_html_dialog), si);
  gtk_signal_connect (GTK_OBJECT (prevbutton), "clicked",
		      GTK_SIGNAL_FUNC (prev_sched), si);
  gtk_signal_connect (GTK_OBJECT (nextbutton), "clicked",
		      GTK_SIGNAL_FUNC (next_sched), si);


  gtk_signal_connect (GTK_OBJECT (okbutton), "clicked",
		      GTK_SIGNAL_FUNC (destroy_win), window);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy_win), window);
  gtk_widget_show(window);

  // this lets the window size itself appropriately first, then adds the scrollbars
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sched_sw), 
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

}

