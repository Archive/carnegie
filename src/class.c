#include <stdio.h>
#include <ghttp.h>
#include <glib.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include "class.h"
#include "timeslot.h"
#include "prefs.h"

#define WIDTH_LIMIT 10

extern GnomeApp *gapp;
extern GtkWidget *statusbar;

extern gchar *global_season;
extern gchar *global_campus;
extern gchar *global_handler;
extern gchar *global_handler_path;
extern gchar *global_error;
extern int global_merge;
extern int global_abbr_sec;

// returns true if two classes are identical, except for section
int are_classes_similar(Class *c1, Class *c2)
{
  int i;

  if(c1==c2) return 1;

  if(strcmp(c1->id, c2->id)) return 0;
  if(strcmp(c1->title, c2->title)) return 0;
  for(i=MONDAY;i<=FRIDAY;i++)
    if(isDay(c1->days, i)!=isDay(c2->days, i)) return 0;
  if(c1->begin != c2->begin) return 0;
  if(c1->end != c2->end) return 0;

  return 1;
}

int are_blocks_similar(Block *b1, Block *b2)
{
  GPtrArray *cgp1, *cgp2;
  int i;
  
  if(!are_classes_similar(b1->lec, b2->lec)) return 0;
  cgp1 = b1->rec;
  cgp2 = b2->rec;
  if(cgp1==NULL && cgp2) return 0;
  if(cgp1 && cgp2==NULL) return 0;
  if(cgp1) {
    if(cgp1->len != cgp2->len) return 0;
    for(i=0;i<cgp1->len;i++)
      if(!are_classes_similar(cgp1->pdata[i], cgp2->pdata[i])) return 0;
  }
  return 1;
}

int similar_block_in_array(GPtrArray *blist, Block *b)
{
  int i;
  for(i=0;i<blist->len;i++)
    if(are_blocks_similar(blist->pdata[i], b)) return i;

  return -1;
}

GPtrArray *merge_similar_blocks(GPtrArray *blist)
{
  Block *b, *next;
  int n, i;
  GPtrArray *gp, *copy;
  gchar *sections;
  gchar *startsection, *endsection;
  Class *sec;
  
  assert(blist);
  gp = g_ptr_array_new();
  copy = g_ptr_array_new();
  for(i=0;i<blist->len;i++)
    g_ptr_array_add(copy, blist->pdata[i]);

  blist = copy;
  while(blist->len>0) {
    b = blist->pdata[0];
    g_ptr_array_remove_index(blist, 0);

    if(b->rec) sec = b->rec->pdata[0];
    else sec = b->lec;
    startsection = g_strdup(sec->section);
    sections = g_strdup(sec->section);
    endsection = NULL;

    while((n=similar_block_in_array(blist, b)) != -1) {
      next = g_ptr_array_remove_index(blist, n);
      if(b->rec) sec = next->rec->pdata[0];
      else sec = next->lec;
      sections = g_strconcat(sections, "/", sec->section, NULL);      
      endsection = g_strdup(sec->section);
    }
    
    if(b->rec) sec = b->rec->pdata[0];
    else sec = b->lec;

    if(strlen(sections)>strlen(b->lec->id)-2 && endsection) {
      sec->abbrsection = g_strconcat(startsection, " ... ", endsection, NULL);
    } else
      sec->abbrsection = sections;
    
    sec->fullsection = sections;

    g_ptr_array_add(gp, b);
  }

  return gp;
}

GPtrArray *flatten_blocks(GPtrArray *blist)
{
  int i, j;
  Block *b;
  GPtrArray *gp;
  GPtrArray *c;

  assert(blist);
  gp = g_ptr_array_new();
  for(i=0;i<blist->len;i++) {
    b = (Block *) blist->pdata[i];
    g_ptr_array_add(gp, (gpointer)b->lec);
    if(b->rec) {
      c = b->rec;
      for(j=0;j<c->len;j++)
	g_ptr_array_add(gp, c->pdata[j]);
    }
  }

  return gp;
}

char *get_twixt_tab(char *data, int tn)
{
  int i;
  char *p, *e, *en;
  char *ans;

  p = data;
  for(i=0;i<tn;i++) {
    p = strstr(p, "\t");
    if(p==NULL) return NULL;
    p++;
  }

  en = strstr(p, "\n");
  e = strstr(p, "\t");

  if(!e) e = en;
  else if(en && en<e) e = en;
  assert(e);

  ans = g_strndup(p, (e-p));
  return ans;
}

char *get_handler_data(char *cls)
{
  GString *gs;
  char tmp[255];
  char *ans;
  FILE *fp;
  char *toopen;

  toopen = g_strconcat("\"", global_handler_path, global_handler, "\" \"",
                      cls, "\" \"", global_season, "\" \"", 
		      global_campus, "\"", NULL); 

  //printf("%s\n", toopen);
  if((fp = popen(toopen, "r")) == NULL) {
    printf("Error with popen() call in get_handler_data().\n");
    exit(1);
  }

  gs = g_string_new(NULL);
  while(fgets(tmp, sizeof(tmp), fp))
    g_string_append(gs, tmp);

  ans = gs->str;
  g_string_free(gs, FALSE);

  pclose(fp);
  return ans;
}

char * parse_handler_line(char *data, Class **cls) 
{
  char *next, *line;
  Class *c;

  if(data[0] == '\t') data++;
  next = strstr(data, "\n");
  assert(next);
  next++;
  line = g_strndup(data, (next-data));

  c = (Class *) malloc(sizeof(Class));

  c->id = get_twixt_tab(line, 0);
  c->title = get_twixt_tab(line, 1);  
  c->section = get_twixt_tab(line, 2);
  c->abbrsection = NULL;
  c->fullsection = g_strdup(c->section);
  c->days = get_twixt_tab(line, 3);
  c->begin = get_timeslot(get_twixt_tab(line, 4), DOWN);
  c->end = get_timeslot(get_twixt_tab(line, 5), UP);

  *cls = c;
  return next;
}

GPtrArray *get_class(char * classnum)
{
  GPtrArray *ans;
  GPtrArray *lastsecgp;
  char *data, *index, *mark;
  char *lastsec;
  Class *c, *lc;
  Block *b;
  int added = 0;

  data = get_handler_data(classnum);

  if(isError(data)) 
    return NULL;

  index = data;
  mark = data;

  // A good indication if something went wrong... no \t's anywhere...
  if(!strstr(data, "\t")) {
    global_error = g_strconcat("Handler OOPS!  Spewed results follow: \n\n", 
			       data, NULL);;
    return NULL;
  }

  ans = g_ptr_array_new();
  lastsec = NULL;

  while(index[0] != '\0') {
    mark = parse_handler_line(index, &c);

    // this is a recitation
    if(index[0] == '\t') {
      if(lastsec && !strcmp(lastsec, c->section)) {
	g_ptr_array_add(lastsecgp, c);
      }
      else {
	b = (Block *) malloc(sizeof(Block));
	b->lec = lc;
	lastsec = c->section;
	lastsecgp = g_ptr_array_new();
	g_ptr_array_add(lastsecgp, c);
	b->rec = lastsecgp;
	g_ptr_array_add(ans, b);
	added++;
      }
    }
    // this is a lecture
    else {
      lc = c;
      if(mark[0] != '\t') {
	b = (Block *) malloc(sizeof(Block));
	b->lec = c;
	b->rec = NULL;
	g_ptr_array_add(ans, b);    	
	added++;
      }
    }
    index = mark;
  }

  if(!added) return NULL;
  //print_blocks(ans);
  return ans;
}

/*
char *getURL (char * url)
{
  ghttp_request * gr = NULL;
  char *result;

  gr = ghttp_request_new();

  if(ghttp_set_uri(gr, url) == -1) {
    printf("Invalid URI.");
    exit(1);
  }

  ghttp_set_header(gr, http_hdr_Connection, "close");

  ghttp_prepare(gr);
  ghttp_process(gr);

  result = g_strndup(ghttp_get_body(gr), ghttp_get_body_len(gr));

  ghttp_request_destroy(gr);

  return result;
}*/

void print_blocks(GPtrArray *gp)
{
  int i;
  Block *b;
  for(i=0;i<gp->len;i++) {
    b = (Block *)gp->pdata[i];
    print_class(b->lec);
    if(b->rec) print_classes(b->rec);
    printf("\n");
  }
}

void print_classes(GPtrArray *gp)
{
  int i;
  for(i=0;i<gp->len;i++)
    print_class(gp->pdata[i]);
}

void print_class(Class *c)
{
  char *b, *e;
  if(c==NULL) {
    printf("printClass() failure!!!!!!!!\n");
    return;
  }
  b = timeslot_str(c->begin);
  e = timeslot_str(c->end);

  printf("%s \t%s \t%s\n", c->id, c->title, c->section);
  printf("\t%s \t%s \t%s\n", c->days, b, e);
}
