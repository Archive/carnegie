#ifndef __GUI_H__
#define __GUI_H__

void setup_screen(GtkWidget *app);
void destroy(GtkWidget *widget, gpointer callback_data);
void install_menu (GtkWidget *omenu, GPtrArray *data, int type);
char *get_twixt_space(char *data, int tn);

#endif
