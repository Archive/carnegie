#include <stdio.h>
#include <string.h>
#include <glib.h>
#include "timeslot.h"

int isDay(char *str, dayslot d)
{
  char *c;
  char daystr[2];

  daystr[1] = '\0';

  switch (d) {
  case MONDAY: 
    daystr[0] = 'M'; break;
  case TUESDAY: 
    daystr[0] = 'T'; break;
  case WEDNESDAY: 
    daystr[0] = 'W'; break;
  case THURSDAY: 
    daystr[0] = 'R'; break;
  case FRIDAY: 
    daystr[0] = 'F'; break;
  }

  c = strstr(str, daystr);

  return !(c==NULL);
}

timeslot get_timeslot(char *t, int round)
{
  char *ts;

  //this makes our string into the format xx00 or xx30  
  ts = g_strdup(t);  
  ts[3] = '0';
  if(round==UP) {
    if(ts[2] != '0' && ts[2] != '3') {
      if(ts[2] > '3') {
	ts[2] = '0';
	if(ts[1] != '9')
	  ts[1]++;
	else {
	  ts[1] = '0';
	  ts[0]++;
	}
      }
      else ts[2] = '3';
    }
  } else {
    if(ts[2] != '0' && ts[2] != '3') {
      if(ts[2] >= '3') ts[2] = '3';
      else ts[2] = '0';
    }
  }

  if(!strncmp(ts, "0600", 4)) return SIX_AM;
  if(!strncmp(ts, "0630", 4)) return SIX_THIRTY_AM;
  if(!strncmp(ts, "0700", 4)) return SEVEN_AM;
  if(!strncmp(ts, "0730", 4)) return SEVEN_THIRTY_AM;
  if(!strncmp(ts, "0800", 4)) return EIGHT_AM;
  if(!strncmp(ts, "0830", 4)) return EIGHT_THIRTY_AM;
  if(!strncmp(ts, "0900", 4)) return NINE_AM;
  if(!strncmp(ts, "0930", 4)) return NINE_THIRTY_AM;
  if(!strncmp(ts, "1000", 4)) return TEN_AM;
  if(!strncmp(ts, "1030", 4)) return TEN_THIRTY_AM;
  if(!strncmp(ts, "1100", 4)) return ELEVEN_AM;
  if(!strncmp(ts, "1130", 4)) return ELEVEN_THIRTY_AM;
  if(!strncmp(ts, "1200", 4)) return TWELVE_PM;
  if(!strncmp(ts, "1230", 4)) return TWELVE_THIRTY_PM;
  if(!strncmp(ts, "1300", 4)) return ONE_PM;
  if(!strncmp(ts, "1330", 4)) return ONE_THIRTY_PM;
  if(!strncmp(ts, "1400", 4)) return TWO_PM;
  if(!strncmp(ts, "1430", 4)) return TWO_THIRTY_PM;
  if(!strncmp(ts, "1500", 4)) return THREE_PM;
  if(!strncmp(ts, "1530", 4)) return THREE_THIRTY_PM;
  if(!strncmp(ts, "1600", 4)) return FOUR_PM;
  if(!strncmp(ts, "1630", 4)) return FOUR_THIRTY_PM;
  if(!strncmp(ts, "1700", 4)) return FIVE_PM;
  if(!strncmp(ts, "1730", 4)) return FIVE_THIRTY_PM;
  if(!strncmp(ts, "1800", 4)) return SIX_PM;
  if(!strncmp(ts, "1830", 4)) return SIX_THIRTY_PM;
  if(!strncmp(ts, "1900", 4)) return SEVEN_PM;
  if(!strncmp(ts, "1930", 4)) return SEVEN_THIRTY_PM;
  if(!strncmp(ts, "2000", 4)) return EIGHT_PM;
  if(!strncmp(ts, "2030", 4)) return EIGHT_THIRTY_PM;
  if(!strncmp(ts, "2100", 4)) return NINE_PM;
  if(!strncmp(ts, "2130", 4)) return NINE_THIRTY_PM;
  if(!strncmp(ts, "2200", 4)) return TEN_PM;
  if(!strncmp(ts, "2230", 4)) return TEN_THIRTY_PM;
  if(!strncmp(ts, "2300", 4)) return ELEVEN_PM;
  if(!strncmp(ts, "2330", 4)) return ELEVEN_THIRTY_PM;
  if(!strncmp(ts, "2400", 4)) return TWELVE_AM;


  printf("\ngetTimeslot(): Unknown time in:\t[%s]\n", ts);
  return END_SLASH_UNKNOWN;
}

char *timeslot_str(timeslot t)
{
  char *str;

  if(t==SIX_AM) str = g_strdup("6:00a");
  else if(t==SIX_THIRTY_AM) str = g_strdup("6:30a");
  else if(t==SEVEN_AM) str = g_strdup("7:00a");
  else if(t==SEVEN_THIRTY_AM) str = g_strdup("7:30a");
  else if(t==EIGHT_AM) str = g_strdup("8:00a");
  else if(t==EIGHT_THIRTY_AM) str = g_strdup("8:30a");
  else if(t==NINE_AM) str = g_strdup("9:00a");
  else if(t==NINE_THIRTY_AM) str = g_strdup("9:30a");
  else if(t==TEN_AM) str = g_strdup("10:00a");
  else if(t==TEN_THIRTY_AM) str = g_strdup("10:30a");
  else if(t==ELEVEN_AM) str = g_strdup("11:00a");
  else if(t==ELEVEN_THIRTY_AM) str = g_strdup("11:30a");
  else if(t==TWELVE_PM) str = g_strdup("12:00p");
  else if(t==TWELVE_THIRTY_PM) str = g_strdup("12:30p");
  else if(t==ONE_PM) str = g_strdup("1:00p");
  else if(t==ONE_THIRTY_PM) str = g_strdup("1:30p");
  else if(t==TWO_PM) str = g_strdup("2:00p");
  else if(t==TWO_THIRTY_PM) str = g_strdup("2:30p");
  else if(t==THREE_PM) str = g_strdup("3:00p");
  else if(t==THREE_THIRTY_PM) str = g_strdup("3:30p");
  else if(t==FOUR_PM) str = g_strdup("4:00p");
  else if(t==FOUR_THIRTY_PM) str = g_strdup("4:30p");
  else if(t==FIVE_PM) str = g_strdup("5:00p");
  else if(t==FIVE_THIRTY_PM) str = g_strdup("5:30p");
  else if(t==SIX_PM) str = g_strdup("6:00p");
  else if(t==SIX_THIRTY_PM) str = g_strdup("6:30p");
  else if(t==SEVEN_PM) str = g_strdup("7:00p");
  else if(t==SEVEN_THIRTY_PM) str = g_strdup("7:30p");
  else if(t==EIGHT_PM) str = g_strdup("8:00p");
  else if(t==EIGHT_THIRTY_PM) str = g_strdup("8:30p");
  else if(t==NINE_PM) str = g_strdup("9:00p");
  else if(t==NINE_THIRTY_PM) str = g_strdup("9:30p");
  else if(t==TEN_PM) str = g_strdup("10:00p");
  else if(t==TEN_THIRTY_PM) str = g_strdup("10:30p");
  else if(t==ELEVEN_PM) str = g_strdup("11:00p");
  else if(t==ELEVEN_THIRTY_PM) str = g_strdup("11:30p");
  else if(t==TWELVE_AM) str = g_strdup("12:00a");
  else str = g_strdup("<unknown>");

  return str;
}


