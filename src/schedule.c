#include <assert.h>
#include "schedule.h"
#include "class.h"

void weekschedule_empty(weekschedule *w)
{
  w->M = 0;
  w->T = 0;
  w->W = 0;
  w->R = 0;
  w->F = 0;
}

int day_add(long *day, dayslot begin, dayslot end)
{
  int i;
  long bitfiddle;
  for(i=begin; i<end; i++) {
    bitfiddle = (1L<<i);
    if((bitfiddle & *day) != 0)
      return -1;
    *day = *day | bitfiddle;
  }

  return 0;
}

int add_class(Class *c, weekschedule *w)
{
  int ret = 0;
  if(isDay(c->days, MONDAY)) {
    ret += day_add(&(w->M), c->begin, c->end);
  }
  if(isDay(c->days, TUESDAY)) {
    ret += day_add(&(w->T), c->begin, c->end);
  }
  if(isDay(c->days, WEDNESDAY)) {
    ret += day_add(&(w->W), c->begin, c->end);
  }
  if(isDay(c->days, THURSDAY)) {
    ret += day_add(&(w->R), c->begin, c->end);
  }
  if(isDay(c->days, FRIDAY)) {
    ret += day_add(&(w->F), c->begin, c->end);
  }

  return ret;
}

int add_block(Block *b, weekschedule *w)
{
  int ret = 0;
  int i;
  Class *l;
  GPtrArray *c;

  l = b->lec;
  c = b->rec;
  ret += add_class(l, w);
  if(c) {
    for(i=0;i<c->len;i++)
      ret += add_class(c->pdata[i], w);
  }

  return ret;
}


/* schedule() returns a GPtrArray of GPtrArray of Class*'s.
   (I.e. something that sched-view knows how to display.)
*/

GPtrArray * schedule(GPtrArray * blocklistlist)
{
  int *counter, *size;
  int i, end, cursor;
  Block *b;
  GPtrArray *whichclass;
  weekschedule ws;
  int ret;
  GPtrArray *gp, *mgp;
  int added;

  added = 0;
  mgp = g_ptr_array_new();

  end = blocklistlist->len - 1;

  counter = (int *) malloc(sizeof(int) * (end + 1));
  size = (int *) malloc(sizeof(int) * (end + 1));

  for(i = 0; i <= end; i++) {
    counter[i] = 0;
    size[i] = ((GPtrArray *) (blocklistlist->pdata[i]))->len;
    //printf("size[%d] = %d\n", i, size[i]);
  }

  cursor = end;

  // Think about the digits on a gas pump!

  while(cursor >= 0) {

    // at this point we have a "snapshot" of the gas pump readout.
    
    /* A quick debug check... 
    for(i = 0; i <= end; i++) 
      printf("%d ", counter[i]);
    printf("\tcursor: %d\tend: %d\n", cursor, end);    
    */
        
    ret = 0;
    weekschedule_empty(&ws);
    for(i = 0; i <= end; i++) {
      whichclass = (GPtrArray *)(blocklistlist->pdata[i]);
      b = (Block *) whichclass->pdata[counter[i]];
      ret += add_block(b, &ws);
    }

    // if this "snapshot" created no scheduling conflicts, remember it.
    if(ret==0) {
      gp = g_ptr_array_new();
      assert(gp);
      
      for(i=0; i <= end; i++) {
	whichclass = (GPtrArray *)(blocklistlist->pdata[i]);
	b = (Block *) whichclass->pdata[counter[i]];
	g_ptr_array_add(gp, b);
      }
      
      added++;
      g_ptr_array_add(mgp, gp);
    }
    

    // size[cursor] is one past the last valid number in the array
    // cursor should always point to the next digit to increment

    if(counter[cursor] >= size[cursor]-1) {
      while(counter[cursor] >= size[cursor]-1 && cursor >= 0) {
	counter[cursor] = 0;
	cursor--;
      }
    }

    if(cursor != -1) counter[cursor]++;

    // zoom forward to the next spot where we can add something
    if(cursor!=end && cursor != -1) {
      cursor++;
      while(cursor!=end && counter[cursor] == size[cursor]-1) cursor++;
      //printf("c: %d\n", cursor);
    }
  }

  counter = NULL;
  size = NULL;
  free(counter);
  free(size);

  if(!added) return NULL;
  return mgp;
}
