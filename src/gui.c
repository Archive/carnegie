#include <gnome.h>
#include <assert.h>
#include <glib.h>

#include "class.h"
#include "main.h"
#include "prefs.h"
#include "gui.h"
#include "schedview.h"
#include "schedule.h"

GtkWidget *clist;
GnomeApp *gapp;
GtkWidget *statusbar;
int numentries;
int rowselected;

GtkWidget *seasons_omenu;
GtkWidget *campus_omenu;
GtkWidget *handlers_omenu;

extern gchar *global_season;
extern gchar *global_campus;
extern gchar *global_handler;
extern gchar *global_handler_path;
extern gchar *global_error;
extern int global_merge;

char *get_twixt_space(char *data, int tn)
{
  int i;
  char *p, *e;
  char *ans;

  p = data;
  for(i=0;i<tn;i++) {
    p = strstr(p, " ");
    if(p==NULL) return NULL;
    p++;
  }

  e = strstr(p, " ");
  if(!e) ans = g_strdup(p);
  else 
    ans = g_strndup(p, (e-p));
  return ans;
}

void season_activated (GtkWidget *widget, gpointer data)
{
  char *season;

  season = gtk_object_get_user_data(GTK_OBJECT(widget));
  global_season = season;
}

void campus_activated (GtkWidget *widget, gpointer data)
{
  char *campus;

  campus = gtk_object_get_user_data(GTK_OBJECT(widget));
  global_campus = campus;
}

void handler_activated (GtkWidget *widget, gpointer data)
{
  char *handler;
  struct handlerinfo *hi;

  handler = gtk_object_get_user_data(GTK_OBJECT(widget));
  global_handler = handler;

  //recreate season & campus menus here; they may change
  hi = get_handlerinfo();
  install_menu(seasons_omenu, hi->seasondata, SEASON);
  install_menu(campus_omenu, hi->campusdata, CAMPUS);
  global_campus = hi->campusdata->pdata[0];
  global_season = hi->seasondata->pdata[0];
}

void install_menu (GtkWidget *omenu, GPtrArray *data, int type) 
{
  GtkWidget *menu;
  int i;
  static GtkWidget **items;

  items = (GtkWidget **) malloc(sizeof(GtkWidget *) * data->len);
  
  menu = gtk_menu_new();
  for(i=0; i<data->len; i++) {
    items[i] = gtk_menu_item_new_with_label((char *)data->pdata[i]);
    gtk_object_set_user_data (GTK_OBJECT (items[i]), data->pdata[i]);
    if(type==CAMPUS)
      gtk_signal_connect (GTK_OBJECT (items[i]), "activate",
			  (GtkSignalFunc) campus_activated,
			  omenu);
    else
      gtk_signal_connect (GTK_OBJECT (items[i]), "activate",
			  (GtkSignalFunc) season_activated,
			  omenu);

    gtk_menu_append (GTK_MENU (menu), items[i]);
    gtk_widget_show (items[i]); 
  }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (omenu), menu);
}

void install_handlers_menu (GtkWidget *omenu) 
{
  GtkWidget *menu;
  int i;
  static GtkWidget **items;
  GPtrArray *handlerdata;

  handlerdata = get_handlers();
  assert(handlerdata->len);

  global_handler = handlerdata->pdata[0];
  items = (GtkWidget **) malloc(sizeof(GtkWidget *) * handlerdata->len);
  
  menu = gtk_menu_new();
  for(i=0; i<handlerdata->len; i++) {
    items[i] = gtk_menu_item_new_with_label((char *)handlerdata->pdata[i]);
    //gtk_object_set_user_data (GTK_OBJECT (items[i]), GINT_TO_POINTER (i));
    gtk_object_set_user_data (GTK_OBJECT (items[i]), handlerdata->pdata[i]);
    gtk_signal_connect (GTK_OBJECT (items[i]), "activate",
			(GtkSignalFunc) handler_activated,
			omenu);
    gtk_menu_append (GTK_MENU (menu), items[i]);
    gtk_widget_show (items[i]); 
  }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (omenu), menu);
  //  gtk_option_menu_set_history (GTK_OPTION_MENU (omenu), active);
}


void ready_to_schedule(GtkWidget *widget, gpointer callback_data)
{
  GPtrArray *b, *gp;
  GPtrArray *result;
  int i;

  if(numentries==0) {
    gnome_app_message(gapp, 
		      "You haven't added any classes yet.\n"
		      "Before scheduling, you must add at least one class!\n"
		      "\nPlease add a class and then click \"schedule\".");
    return;
  }

  b = g_ptr_array_new();
  for(i=0;i<numentries;i++) {
    gp = gtk_clist_get_row_data(GTK_CLIST(clist), i);
    if(global_merge) gp = merge_similar_blocks(gp);
    g_ptr_array_add(b, gp);
  }
  result = schedule(b);

  if(!result) {
    gnome_app_message(gapp, 
		      "Sorry!\n\n"
		      "There are no available schedules "
		      "with the courses you selected.\n"
		      "Try removing a course...");
    return;
  }

  setup_schedview(result);
}

void view_class(GtkWidget *widget, gpointer data)
{
  GPtrArray *mgp;
  GPtrArray *gp = gtk_clist_get_row_data(GTK_CLIST(clist), rowselected);
  //gint row = GPOINTER_TO_INT(data);

  mgp = g_ptr_array_new();
  if(global_merge) gp = merge_similar_blocks(gp);
  g_ptr_array_add(mgp, gp);
  gp = schedule(mgp);
  setup_schedview(gp);
}

void remove_class(GtkWidget *widget, gpointer data)
{
  //gint row = GPOINTER_TO_INT(data);
  //FIXME:  why doesn't the above work?!?

  gtk_clist_remove(GTK_CLIST(clist), rowselected);
  numentries--;
}

void selection_made(GtkWidget *widget, gint row, gint column,
		    GdkEventButton *event, gpointer data)
{
  GtkWidget * popup;
  GnomeUIInfo clistmenu[3];

  rowselected = row; // <-- damn ugly
  clistmenu[0].type = GNOME_APP_UI_ITEM;
  clistmenu[0].label = "_View class schedule";
  clistmenu[0].hint = "View available class lectures and recitations";
  clistmenu[0].moreinfo = view_class;
  clistmenu[0].user_data = GINT_TO_POINTER(row);
  clistmenu[0].unused_data = NULL;
  clistmenu[0].pixmap_type = GNOME_APP_PIXMAP_NONE;
  clistmenu[0].pixmap_info = NULL;
  clistmenu[0].accelerator_key = 0;

  clistmenu[1].type = GNOME_APP_UI_ITEM;
  clistmenu[1].label = "_Remove class";
  clistmenu[1].hint = "Remove class from list";
  clistmenu[1].moreinfo = remove_class;
  clistmenu[1].user_data = GINT_TO_POINTER(row);
  clistmenu[1].unused_data = NULL;
  clistmenu[1].pixmap_type = GNOME_APP_PIXMAP_NONE;
  clistmenu[1].pixmap_info = NULL;
  clistmenu[1].accelerator_key = 0;
  clistmenu[2].type = GNOME_APP_UI_ENDOFINFO;

  popup = gnome_popup_menu_new(clistmenu);
  gnome_app_install_appbar_menu_hints ((GnomeAppBar*) statusbar, clistmenu);
  gnome_popup_menu_do_popup(popup, NULL, NULL, NULL, NULL );

}

void website(GtkWidget *widget, gpointer callback_data)
{
  gnome_url_show("http://www.tunison.net/code/carnegie");
}

void destroy(GtkWidget *widget, gpointer callback_data)
{
  gtk_main_quit();
}

void about_cb(GtkWidget *widget, gpointer callback_data ) 
{
  const gchar *authors[] = {"John Tunison <nosinut@cmu.edu>", 
			    NULL};
  GtkWidget *about = gnome_about_new ( _("Carnegie"), VERSION,
		       _("Copyright 1999."),
                       authors,
		       "Carnegie is a class scheduler. "
		       "Released under the terms of the GPL.\n"
		       "The following people have contributed "
		       "handlers and/or patches:  "
		       "Yatish Patel <ypatel@andrew.cmu.edu>, "
                       "Rich Seymour <rjs51@columbia.edu>, "
		       "Stephen Webb <spidey@dodds.net>\n",
                        NULL);
  gtk_widget_show (about);
}

void entry_enter(GtkWidget *widget, GtkWidget *entry)
{
  gchar *entry_text;
  int i, j;
  GPtrArray *gp;
  Block *b;
  Class *c;
  char tmp[80];
  char *line[3];
  char *classnum;

  entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
  j = 0;

  while((classnum = get_twixt_space(entry_text, j)) != NULL) {
    if((gp = get_class(classnum)) == NULL) {
      if(global_error)
	gnome_app_message(gapp, global_error);
      else	
	gnome_app_message(gapp, "Sorry, that class does not exist.\n");
      return;
    }

    gtk_entry_set_text(GTK_ENTRY(entry), "");
    //print_blocks(gp);
    
    /*print_blocks(gp);*/
    if((b = (Block *) gp->pdata[0]) == NULL) return;
    if((c = (Class *) b->lec) == NULL) return;
    sprintf(tmp, "%d", gp->len);
    line[0] = c->id;
    line[1] = tmp;
    line[2] = c->title;
    i = gtk_clist_append( GTK_CLIST(clist), line);
    gtk_clist_set_row_data(GTK_CLIST(clist), i, gp);
    numentries++;
    j++;
  }
}

void setup_screen(GtkWidget *app)
{
  GtkWidget *vbox, *hbox;
  GtkWidget *omenu_hbox;
  GtkWidget *addbutton, *schedbutton;
  GtkWidget *entry;
  GtkWidget *scrolled_window;
  GtkWidget *label;
  struct handlerinfo *hi;
  char *titles[3] = { "ID", 
		      "BLOCKS",
		      "TITLE" };

  GnomeUIInfo optionmenu[] = {
    GNOMEUIINFO_MENU_PREFERENCES_ITEM(do_prefs, NULL),
    GNOMEUIINFO_MENU_EXIT_ITEM(destroy, NULL),
    GNOMEUIINFO_END
  };

  GnomeUIInfo helpmenu[] = {
    {GNOME_APP_UI_ITEM, 
     N_("_Website"), N_("Visit the Carnegie website"),
     website, NULL, NULL, 
     GNOME_APP_PIXMAP_NONE, NULL,
     0, 0, NULL},
    GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb, NULL),
    GNOMEUIINFO_END
  };

 GnomeUIInfo mainmenu[] = {
   GNOMEUIINFO_SUBTREE(N_("_Option"), optionmenu),
   GNOMEUIINFO_SUBTREE(N_("_Help"), helpmenu),
   GNOMEUIINFO_END
 };

 numentries=0;
 gapp = (GnomeApp*) app;
 gnome_app_create_menus((GnomeApp *)app, mainmenu);

 statusbar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_NEVER);
 gnome_app_set_statusbar ((GnomeApp *)app, statusbar);
 gnome_app_install_appbar_menu_hints ((GnomeAppBar*) statusbar, mainmenu);

  vbox=gtk_vbox_new(FALSE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 20);
  gnome_app_set_contents ( GNOME_APP (app), vbox);
  gtk_widget_show(vbox);

  omenu_hbox=gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), omenu_hbox, FALSE, FALSE, 10);
  gtk_widget_show(omenu_hbox);

  label = gtk_label_new("College:");
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  handlers_omenu = gtk_option_menu_new();
  install_handlers_menu(handlers_omenu);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), handlers_omenu, FALSE, FALSE, 0);
  gtk_widget_show(handlers_omenu);

  label = gtk_label_new("  Campus:");
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  hi = get_handlerinfo();
  global_campus = hi->campusdata->pdata[0];
  global_season = hi->seasondata->pdata[0];

  campus_omenu = gtk_option_menu_new();
  install_menu(campus_omenu, hi->campusdata, CAMPUS);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), campus_omenu, FALSE, FALSE, 0);
  gtk_widget_show(campus_omenu);

  label = gtk_label_new("   Season:");
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  seasons_omenu = gtk_option_menu_new();
  install_menu(seasons_omenu, hi->seasondata, SEASON);
  gtk_box_pack_start(GTK_BOX(omenu_hbox), seasons_omenu, FALSE, FALSE, 0);
  gtk_widget_show(seasons_omenu);

  scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), 
				 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(scrolled_window, 0, 120);
  gtk_box_pack_start(GTK_BOX(vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show(scrolled_window);
 
  clist = gtk_clist_new_with_titles(3, titles);
  gtk_clist_set_column_width (GTK_CLIST(clist), 0, 75);
  gtk_clist_set_column_width (GTK_CLIST(clist), 1, 50);
  gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_SINGLE );
  gtk_clist_column_titles_passive(GTK_CLIST(clist));
  gtk_clist_column_titles_show(GTK_CLIST(clist));
  
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), clist);
  gtk_widget_show(clist);

  hbox=gtk_hbox_new(FALSE, 5);
  //  gtk_container_set_border_width(GTK_CONTAINER(hbox), 10);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 10);
  gtk_widget_show(hbox);

  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
  gtk_widget_show(entry);
  
  addbutton = gtk_button_new_with_label ("Add Course");
  gtk_box_pack_start(GTK_BOX(hbox), addbutton, FALSE, FALSE, 0);
  gtk_widget_show(addbutton);

  schedbutton = gtk_button_new_with_label ("  Schedule!  ");
  gtk_box_pack_end(GTK_BOX(hbox), schedbutton, FALSE, FALSE, 0);
  gtk_widget_show(schedbutton);

  gtk_signal_connect(GTK_OBJECT(entry), "activate",
		     GTK_SIGNAL_FUNC(entry_enter),
		     entry);
  gtk_signal_connect (GTK_OBJECT (addbutton), "clicked",
		      GTK_SIGNAL_FUNC (entry_enter), entry);
  gtk_signal_connect (GTK_OBJECT (schedbutton), "clicked",
		      GTK_SIGNAL_FUNC (ready_to_schedule), NULL);
  gtk_signal_connect(GTK_OBJECT(clist), "select_row",
		     GTK_SIGNAL_FUNC(selection_made),
		     NULL);

  gtk_widget_grab_focus( entry );
  gnome_app_flash ((GnomeApp *) app, "Type a class number and press return!");
  
}

