#include <gnome.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>

#include "prefs.h"
#include "class.h"
#include "gui.h"
#include "main.h"

extern gchar *global_season;
extern gchar *global_campus;
extern gchar *global_handler_path;
extern gchar *global_handler;
extern gchar *global_error;
extern int global_merge;
extern int global_abbr_sec;

GtkWidget *mergecheck;
GtkWidget *abbrseccheck;

void init_prefs(void)
{
  // note that global_handler, global_season, and global_campus
  // will all be set in gui.c, resulting in one less popen().

  global_handler = NULL;
  global_campus = NULL;
  global_season = NULL;
  global_merge = TRUE;
  global_abbr_sec = FALSE;

  global_handler_path = g_strdup(HANDLER_PATH);
  global_error = NULL;
}

int isError(char *txt)
{
  int ret = 0;

  if(!strncmp(txt, "error: ", 7)) {
    ret = 1;
    global_error = g_strdup(txt+7);
  }

  return ret;
}

void destroy_prefs(GtkWidget *widget, gpointer cb_data)
{
  gtk_widget_destroy(GTK_WIDGET(cb_data));
}

void ok_prefs(GtkWidget *widget, gpointer cb_data)
{
  global_merge = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mergecheck));
  global_abbr_sec = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(abbrseccheck));

  gtk_widget_destroy(GTK_WIDGET(cb_data));
}

GPtrArray *get_handlers(void)
{
  DIR *dir;
  struct dirent *ent;
  GPtrArray *handlers;
  char *copybuf;
  
  if(!(dir = opendir(global_handler_path))) {
    perror("get_handler_data() opendir failure");
    exit(1);
  }

  handlers = g_ptr_array_new();
  errno = 0;
  while((ent = readdir(dir))) {
    if(ent->d_name[0] != '.') {
      copybuf = g_strdup(ent->d_name);
      g_ptr_array_add(handlers, copybuf);
    }

    // reset errno as puts() could modify it
    errno = 0;
  }

  assert(!errno);
  closedir(dir);
  return handlers;
}

struct handlerinfo *get_handlerinfo(void)
{
  GString *gs;
  char tmp[255];
  FILE *fp;
  char *toopen, *newfield, *ans;
  char *campusline, *seasonline;
  struct handlerinfo *hi;
  int i;

  toopen = g_strconcat("\"", global_handler_path, global_handler, 
		       "\" handlerinfo", NULL); 

  if((fp = popen(toopen, "r")) == NULL) {
    printf("Error with popen() call in get_handlerinfo().\n");
    exit(1);
  }

  gs = g_string_new(NULL);
  while(fgets(tmp, sizeof(tmp), fp))
    g_string_append(gs, tmp);

  ans = gs->str;
  g_string_free(gs, FALSE);
  pclose(fp);

  hi = (struct handlerinfo *) malloc(sizeof(struct handlerinfo));
  hi->seasondata = g_ptr_array_new();
  hi->campusdata = g_ptr_array_new();

  campusline = strstr(ans, "\n");
  assert(seasonline);
  campusline = campusline + 1;
  seasonline = g_strndup(ans, campusline-ans);

  i = 0;
  do {
    newfield = get_twixt_tab(seasonline, i);
    if(newfield) {
      g_ptr_array_add(hi->seasondata, newfield);
    }
    i++;
  } while(newfield);

  i = 0;
  do {
    newfield = get_twixt_tab(campusline, i);
    if(newfield) {
      g_ptr_array_add(hi->campusdata, newfield);
    }
    i++;
  } while(newfield);

  assert((GPtrArray *) (hi->campusdata)->len);
  assert((GPtrArray *) (hi->seasondata)->len);
  free(seasonline);
  free(ans);

  return hi;
}

void do_prefs(void)
{
  GtkWidget *window;
  GtkWidget *vbox, *hbox;
  GtkWidget *okbutton, *cancelbutton;
  GtkWidget *table;

  window = gtk_window_new(GTK_WINDOW_DIALOG);
  gtk_container_set_border_width(GTK_CONTAINER(window), 20);
  gtk_window_set_title (GTK_WINDOW (window), "carnegie preferences");

  vbox=gtk_vbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(window), vbox);
  gtk_widget_show(vbox);

  table = gtk_table_new(3, 3, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);
  gtk_widget_show(table);

  mergecheck = gtk_check_button_new_with_label("Merge blocks");
  gtk_table_attach_defaults(GTK_TABLE(table), mergecheck, 0, 1, 0, 1);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mergecheck), global_merge);
  gtk_widget_show(mergecheck);

  abbrseccheck = gtk_check_button_new_with_label("Abbreviate long section groups");
  gtk_table_attach_defaults(GTK_TABLE(table), abbrseccheck, 0, 1, 1, 2);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(abbrseccheck), global_abbr_sec);
  gtk_widget_show(abbrseccheck);

  hbox=gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_show(hbox);

  okbutton = gnome_stock_button(GNOME_STOCK_BUTTON_OK);
  gtk_box_pack_start(GTK_BOX(hbox), okbutton, FALSE, TRUE, 0);
  gtk_widget_show(okbutton);

  cancelbutton = gnome_stock_button(GNOME_STOCK_BUTTON_CANCEL);
  gtk_box_pack_start(GTK_BOX(hbox), cancelbutton, FALSE, TRUE, 0);
  gtk_widget_show(cancelbutton);

  gtk_signal_connect (GTK_OBJECT (okbutton), "clicked",
		      GTK_SIGNAL_FUNC (ok_prefs), window);
  gtk_signal_connect (GTK_OBJECT (cancelbutton), "clicked",
		      GTK_SIGNAL_FUNC (destroy_prefs), window);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy_prefs), window);
  gtk_widget_show(window);
}
