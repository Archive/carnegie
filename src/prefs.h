#ifndef __PREFS_H__
#define __PREFS_H__

void init_prefs(void);
void do_prefs(void);
GPtrArray *get_data(int type);
struct handlerinfo *get_handlerinfo(void);
GPtrArray *get_handlers(void);
int isError(char *txt);

struct handlerinfo {
  GPtrArray *seasondata;
  GPtrArray *campusdata;
};

#endif
