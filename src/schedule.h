#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__

#include "timeslot.h"
#include <glib.h>

struct _weekschedule {
  long M;
  long T;
  long W;
  long R;
  long F;
};

typedef struct _weekschedule weekschedule;

GPtrArray * schedule(GPtrArray * blocklist);

#endif // __SCHEDULE_H__
